<?php

namespace Megacoders\ShoppingBundle\Service;

use Megacoders\ShoppingBundle\Entity\ShoppingOrder;
use Megacoders\ShoppingBundle\Entity\ShoppingPayment;
use Megacoders\ShoppingBundle\Manager\ShoppingOrderManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Translation\TranslatorInterface;

abstract class AbstractShoppingPaymentService
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * AbstractShoppingPaymentService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->translator = $container->get('translator');
    }

    /**
     * {@inheritdoc}
     */
    public function handleDoneRequest(Request $request)
    {
        $orderId = $request->get('order_id');
        /** @var ShoppingOrder $order */
        $order = $this->findOrder($orderId);

        $this->checkPaymentSuccessResponse($order, $request);

        /** @var ShoppingOrderManager $orderManager */
        $orderManager = $this->container->get('shopping.manager.shopping_order_manager');

        if ($order->getStatus() != ShoppingOrder::STATUS_CONFIRMED) {
            $orderManager->confirmOrder($order);
        }

        $this->applyPayment($order, $request);
    }

    /**
     * {@inheritdoc}
     */
    public function handleFailRequest(Request $request, $cancelOrder = true)
    {
        $orderId = $request->get('order_id');
        $order = $this->findOrder($orderId);

        $this->checkPaymentFailResponse($order, $request);

        if ($cancelOrder) {
            $this->container->get('shopping.manager.shopping_order_manager')
                ->cancelOrder($order);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function handlePendingRequest(Request $request)
    {
        $orderId = $request->get('order_id');
        /** @var ShoppingOrder $order */
        $order = $this->findOrder($orderId);

        $this->checkPaymentPendingResponse($order, $request);

        /** @var ShoppingOrderManager $orderManager */
        $orderManager = $this->container->get('shopping.manager.shopping_order_manager');

        if ($order->getStatus() != ShoppingOrder::STATUS_PENDING) {
            $orderManager->confirmOrder($order);
            $orderManager->pendingOrder($order);
        }
    }

    /**
     * @param ShoppingOrder $order
     * @param Request $request
     */
    public function checkPaymentSuccessResponse(ShoppingOrder $order, Request $request)
    {
        /** @var Session $session */
        $session = $this->container->get('session');

        if ($session->get('order_id') != $order->getId()) {
            throw new BadRequestHttpException($this->translator->trans('modules.shopping.errors.order_wrong_number'));
        }
    }

    /**
     * @param ShoppingOrder $order
     * @param Request $request
     */
    public function checkPaymentFailResponse(ShoppingOrder $order, Request $request)
    {
        /** @var Session $session */
        $session = $this->container->get('session');

        if ($session->get('order_id') != $order->getId()) {
            throw new BadRequestHttpException($this->translator->trans('modules.shopping.errors.order_wrong_number'));
        }
    }

    /**
     * @param ShoppingOrder $order
     * @param Request $request
     */
    public function checkPaymentPendingResponse(ShoppingOrder $order, Request $request)
    {
        /** @var Session $session */
        $session = $this->container->get('session');

        if ($session->get('order_id') != $order->getId()) {
            throw new BadRequestHttpException($this->translator->trans('modules.shopping.errors.order_wrong_number'));
        }
    }

    /**
     * @param int $orderId
     * @return null|ShoppingOrder
     */
    protected function findOrder($orderId)
    {
        return $this->container->get('shopping.manager.shopping_manager')
            ->findOrder($orderId);
    }

    /**
     * @param ShoppingOrder $order
     * @param Request       $request
     */
    public function applyPayment(ShoppingOrder $order, Request $request)
    {
        $amount = $order->getPrice();
        $this->container->get('shopping.manager.shopping_payment_manager')
            ->createPayment($order, $amount, ShoppingPayment::METHOD_PAYMENT_SERVICE);
    }
}