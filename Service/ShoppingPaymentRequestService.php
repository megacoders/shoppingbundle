<?php

namespace Megacoders\ShoppingBundle\Service;


use Megacoders\ShoppingBundle\Entity\ShoppingOrder;
use Megacoders\ShoppingBundle\Exception\PaymentException;
use Symfony\Component\HttpFoundation\Response;

interface ShoppingPaymentRequestService extends ShoppingPaymentService
{

    /**
     * @param ShoppingOrder $order
     * @param string $doneUrl
     * @param string $failUrl
     * @param string $notificationUrl
     * @return Response
     * @throws PaymentException
     */
    function makePaymentRequest(ShoppingOrder $order, $doneUrl = '', $failUrl = '', $notificationUrl = '');

}
