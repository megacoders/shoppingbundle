<?php
namespace Megacoders\ShoppingBundle\Service;

use Megacoders\ShoppingBundle\Entity\ShoppingOrder;
use Symfony\Component\Form\Form;

interface ShoppingPaymentRedirectService extends ShoppingPaymentService
{
    /**
     * @param ShoppingOrder $order
     * @param string $doneUrl
     * @param string $failUrl
     * @param string $pendingUrl
     * @return Form
     */
    function getRedirectForm(ShoppingOrder  $order, $doneUrl = '', $failUrl = '', $pendingUrl = '' );
}