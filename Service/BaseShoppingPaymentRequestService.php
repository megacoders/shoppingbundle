<?php

namespace Megacoders\ShoppingBundle\Service;

use Megacoders\ShoppingBundle\Entity\ShoppingOrder;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;


class BaseShoppingPaymentRequestService extends AbstractShoppingPaymentService  implements ShoppingPaymentRequestService
{

    /**
     * {@inheritdoc}
     */
    public function makePaymentRequest(ShoppingOrder $order, $doneUrl = '', $failUrl = '', $notificationUrl = '')
    {
        /** @var Session $session */
        $session = $this->container->get('session');
        $session->set('order_id', $order->getId());

        return new RedirectResponse($doneUrl);
    }

}
