<?php

namespace Megacoders\ShoppingBundle\Service;

use Megacoders\ShoppingBundle\Entity\ShoppingOrder;
use Symfony\Component\HttpFoundation\Request;

interface ShoppingPaymentService
{
    /**
     * @param Request $request
     */
    function handleDoneRequest(Request $request);

    /**
     * @param Request $request
     * @param bool    $cancelOrder
     */
    function handleFailRequest(Request $request, $cancelOrder);

    /**
     * @param Request $request
     */
    function handlePendingRequest(Request $request);

    /**
     * @param ShoppingOrder $order
     * @param Request $request
     */
    function checkPaymentSuccessResponse(ShoppingOrder $order, Request $request);

    /**
     * @param ShoppingOrder $order
     * @param Request $request
     */
    function checkPaymentFailResponse(ShoppingOrder $order, Request $request);

    /**
     * @param ShoppingOrder $order
     * @param Request $request
     */
    function checkPaymentPendingResponse(ShoppingOrder $order, Request $request);

    /**
     * @param ShoppingOrder $order
     * @param Request       $request
     */
    function applyPayment(ShoppingOrder $order, Request $request);
}
