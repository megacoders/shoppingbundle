<?php

namespace Megacoders\ShoppingBundle\Service;

use Megacoders\ShoppingBundle\Entity\ShoppingOrder;
use Symfony\Component\Form\Form;

class BaseShoppingPaymentRedirectService extends AbstractShoppingPaymentService implements ShoppingPaymentRedirectService
{
    /**
     * @param ShoppingOrder $order
     * @param string $doneUrl
     * @param string $failUrl
     * @param string $pendingUrl
     * @return Form
     */
    public function getRedirectForm(ShoppingOrder  $order, $doneUrl = '', $failUrl = '', $pendingUrl = '')
    {
        return $this->container->get('form.factory')
            ->createNamedBuilder('', 'form')->getForm();
    }
}