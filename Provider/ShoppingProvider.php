<?php

namespace Megacoders\ShoppingBundle\Provider;


use Megacoders\ShoppingBundle\Model\ShoppingProduct;

interface ShoppingProvider
{
    /**
     * @param string $entityClass
     * @return bool
     */
    function supports($entityClass);

    /**
     * @param ShoppingProduct $product
     * @return array
     */
    function getProductDescription(ShoppingProduct $product);

    /**
     * @param ShoppingProduct $product
     * @return bool
     */
    function isProductQuantityEditable(ShoppingProduct $product);

    /**
     * @param ShoppingProduct $product
     * @param int             $orderId
     * @return bool
     */
    function isProductAvailable(ShoppingProduct $product, $orderId);

    /**
     * @param ShoppingProduct $product
     * @return bool
     */
    function isProductParametersEditable(ShoppingProduct $product);

    /**
     * @param ShoppingProduct $product
     * @param array $parameters
     * @return ShoppingProduct
     */
    function updateProductParameters(ShoppingProduct $product, array $parameters);

}
