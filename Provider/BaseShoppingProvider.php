<?php

namespace Megacoders\ShoppingBundle\Provider;

use Megacoders\ShoppingBundle\Model\ShoppingProduct;

abstract class BaseShoppingProvider implements ShoppingProvider
{

    /**
     * {@inheritdoc}
     */
    function isProductQuantityEditable(ShoppingProduct $product)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    function isProductParametersEditable(ShoppingProduct $product)
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    function isProductAvailable(ShoppingProduct $product, $orderId = null)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    function updateProductParameters(ShoppingProduct $product, array $parameters)
    {
        return $product;
    }

}
