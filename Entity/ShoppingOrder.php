<?php

namespace Megacoders\ShoppingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Megacoders\ShoppingBundle\Model\ShoppingProduct;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Megacoders\AdminBundle\Entity\User;


/**
 * @ORM\Entity
 * @ORM\Table(name="shopping_order")
 */
class ShoppingOrder
{
    const DATE_FORMAT = 'Y-m-d H:i:s';

    const STATUS_CREATED = 'CREATED';

    const STATUS_CANCELED = 'CANCELED';

    const STATUS_CONFIRMED = 'CONFIRMED';

    const STATUS_PAID = 'PAID';

    const STATUS_PENDING = 'PENDING';

    const STATUSES_NAMES = [
        self::STATUS_CREATED => 'admin.entities.shopping_order.status_created',
        self::STATUS_CANCELED => 'admin.entities.shopping_order.status_canceled',
        self::STATUS_PAID => 'admin.entities.shopping_order.status_paid',
        self::STATUS_CONFIRMED => 'admin.entities.shopping_order.status_confirmed',
        self::STATUS_PENDING => 'admin.entities.shopping_order.status_pending'
    ];

    const PARAMETER_LOCALE = 'locale';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     * @var \DateTime
     */
    private $date;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @var string
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="ShoppingAccount", cascade={"persist"})
     * @Assert\NotBlank()
     * @var ShoppingAccount
     */
    private $account;

    /**
     * @ORM\OneToMany(targetEntity="ShoppingOrderItem", mappedBy="order", cascade={"persist"}, orphanRemoval=true)
     * @var ArrayCollection
     */
    private $items;

    /**
     * @ORM\ManyToOne(targetEntity="Megacoders\AdminBundle\Entity\User")
     * @var User
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="ShoppingPayment", mappedBy="order", orphanRemoval=true)
     * @var ArrayCollection
     */
    private $payments;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @var array
     */
    private $parameters = [];

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     * @var float
     */
    private $price = 0;

    /**
     * ShoppingOrder constructor.
     */
    public function __construct()
    {
        $this->date = new \DateTime();
        $this->status = self::STATUS_CREATED;
        $this->items = new ArrayCollection();
        $this->payments = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ShoppingOrder
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return ShoppingOrder
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string|null
     */
    public function getStatusName()
    {
        $types = self::STATUSES_NAMES;

        if (isset($types[$this->getStatus()])) {
            return $types[$this->getStatus()];
        }

        return null;
    }

    /**
     * @param string $status
     * @return ShoppingOrder
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return ShoppingAccount
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param ShoppingAccount $account
     * @return ShoppingOrder
     */
    public function setAccount(ShoppingAccount $account)
    {
        $this->account = $account;
        return $this;
    }

    /**
     * @return ArrayCollection|ShoppingProduct[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param ArrayCollection $items
     * @return ShoppingOrder
     */
    public function setItems(ArrayCollection $items)
    {
        $this->items->clear();

        foreach ($items as $item) {
            $this->addItem($item);
        }

        return $this;
    }

    /**
     * @param ShoppingOrderItem $item
     * @return ShoppingOrder
     */
    public function addItem(ShoppingOrderItem $item)
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
            $item->setOrder($this);
        }

        return $this;
    }

    /**
     * @param ShoppingOrderItem $item
     * @return ShoppingOrder
     */
    public function removeItem(ShoppingOrderItem $item)
    {
        $this->items->removeElement($item);
        return $this;
    }

    /**
     * @return $this
     */
    public function removeAllItems()
    {
        $this->items->clear();
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return ShoppingOrder
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return ShoppingOrder
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @param ArrayCollection $payments
     * @return ShoppingOrder
     */
    public function setPayments(ArrayCollection $payments)
    {
        $this->payments->clear();

        foreach ($payments as $payment) {
            $this->addPayment($payment);
        }

        return $this;
    }

    /**
     * @param ShoppingPayment $payment
     * @return $this
     */
    public function addPayment(ShoppingPayment $payment)
    {
        if (!$this->payments->contains($payment)) {
            $this->payments->add($payment);
            $payment->setOrder($this);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    function getParameter($name, $default = null)
    {
        return isset($this->parameters[$name]) ? $this->parameters[$name] : $default;
    }

    /**
     * @param array $parameters
     * @return ShoppingOrder
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return ShoppingOrder
     */
    public function setParameter($name, $value)
    {
        $this->parameters[$name] = $value;
        return $this;
    }

    /**
     * @param string $locale
     * @return ShoppingOrder
     */
    public function setLocale($locale)
    {
        $this->setParameter(self::PARAMETER_LOCALE, $locale);
        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->getParameter(self::PARAMETER_LOCALE);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $date = $this->getDate();

        return $date ? $date->format(self::DATE_FORMAT) : '';
    }
}
