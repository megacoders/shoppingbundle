<?php

namespace Megacoders\ShoppingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Megacoders\ShoppingBundle\Model\EntityDescriptor;
use Megacoders\ShoppingBundle\Model\ShoppingProduct;

/**
 * @ORM\Entity
 * @ORM\Table(name="shopping_order_item")
 */
class ShoppingOrderItem implements ShoppingProduct
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ShoppingOrder", inversedBy="items")
     * @Assert\NotBlank()
     * @var ShoppingOrder
     */
    private $order;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @var string
     */
    private $entityDescriptorString;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $image;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     * @var float
     */
    private $price = 0;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     * @var int
     */
    private $quantity = 1;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @var array
     */
    private $parameters = [];

    /**
     * ShoppingOrderItem constructor.
     * @param ShoppingProduct $product
     */
    public function __construct(ShoppingProduct $product)
    {
        $this
            ->setEntityDescriptor($product->getEntityDescriptor())
            ->setName($product->getName())
            ->setDescription($product->getDescription())
            ->setImage($product->getImage())
            ->setPrice($product->getPrice())
            ->setQuantity($product->getQuantity())
            ->setParameters($product->getParameters())
        ;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ShoppingOrderItem
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return ShoppingOrder
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param ShoppingOrder $order
     * @return ShoppingOrderItem
     */
    public function setOrder(ShoppingOrder $order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return EntityDescriptor
     */
    public function getEntityDescriptor()
    {
        list($entityClass, $entityId) = explode('#', $this->entityDescriptorString, 2);
        return new EntityDescriptor($entityClass, $entityId);
    }

    /**
     * @param EntityDescriptor $entityDescriptor
     * @return ShoppingOrderItem
     */
    public function setEntityDescriptor(EntityDescriptor $entityDescriptor)
    {
        $this->entityDescriptorString = $entityDescriptor->__toString();
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ShoppingOrderItem
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return ShoppingOrderItem
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return ShoppingOrderItem
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return ShoppingOrderItem
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return ShoppingOrderItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return float
     */
    function getTotalPrice()
    {
        return $this->price * $this->quantity;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    function getParameter($name, $default = null)
    {
        return isset($this->parameters[$name]) ? $this->parameters[$name] : $default;
    }

    /**
     * @param array $parameters
     * @return ShoppingOrderItem
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return ShoppingOrderItem
     */
    public function setParameter($name, $value)
    {
        $this->parameters[$name] = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }
}
