<?php

namespace Megacoders\ShoppingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Megacoders\AdminBundle\Entity\User;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="shopping_account")
 */
class ShoppingAccount
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @var string
     */
    private $firstName;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @var string
     */
    private $lastName;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\Email()
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=40, unique=true)
     * @var string
     */
    private $hash;

    /**
     * ShoppingAccount constructor.
     * @param User|null $user
     */
    public function __construct(User $user = null)
    {
        if ($user !== null) {
            $this->firstName = $user->getFirstName();
            $this->lastName = $user->getLastName();
            $this->email = $user->getEmail();
            $this->phone = $user->getPhone();

            $this->updateHash();
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ShoppingAccount
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return ShoppingAccount
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        $this->updateHash();
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return ShoppingAccount
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        $this->updateHash();
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return implode(' ', [$this->lastName, $this->firstName]);
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return ShoppingAccount
     */
    public function setEmail($email)
    {
        $this->email = $email;
        $this->updateHash();
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return ShoppingAccount
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        $this->updateHash();
        return $this;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateHash()
    {
        $this->hash = sha1(implode('|', [$this->firstName, $this->lastName, $this->email, $this->phone]));
    }

}
