<?php
namespace Megacoders\ShoppingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="shopping_payment")
 */
class ShoppingPayment
{
    const DATE_FORMAT = 'Y-m-d H:i:s';

    const METHOD_PAYMENT_SERVICE = 'PAYMENT_SERVICE';

    const METHOD_CASH = 'CASH';

    const METHODS_NAMES = [
        self::METHOD_PAYMENT_SERVICE => 'admin.entities.shopping_payment.method_payment_service',
        self::METHOD_CASH => 'admin.entities.shopping_payment.method_cash'
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     * @var \DateTime
     */
    private $date;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @var string
     */
    private $method;

    /**
     * @ORM\ManyToOne(targetEntity="ShoppingOrder", inversedBy="payments")
     * @Assert\NotBlank()
     * @var ShoppingOrder
     */
    private $order;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     * @var float
     */
    private $amount;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     * @var array
     */
    private $info;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $serviceUniqueId;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod(string $method)
    {
        $this->method = $method;
    }

    /**
     * @return ShoppingOrder
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param ShoppingOrder $order
     */
    public function setOrder(ShoppingOrder $order)
    {
        $this->order = $order;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return array
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param array $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return string
     */
    public function getServiceUniqueId()
    {
        return $this->serviceUniqueId;
    }

    /**
     * @param string $serviceUniqueId
     */
    public function setServiceUniqueId($serviceUniqueId)
    {
        $this->serviceUniqueId = $serviceUniqueId;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return implode(', ', [
            $this->date->format(self::DATE_FORMAT),
            $this->amount,
            $this->method
        ]);
    }
}