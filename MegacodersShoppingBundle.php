<?php

namespace Megacoders\ShoppingBundle;


use Megacoders\ShoppingBundle\DependencyInjection\Compiler\ShoppingProviderPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MegacodersShoppingBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new ShoppingProviderPass());
    }
}
