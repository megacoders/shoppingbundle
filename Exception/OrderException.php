<?php

namespace Megacoders\ShoppingBundle\Exception;


class OrderException extends \Exception
{
    /**
     * OrderException constructor.
     * @param string $message
     */
    public function __construct($message = "")
    {
        parent::__construct($message, 0, null);
    }
}