<?php

namespace Megacoders\ShoppingBundle\Exception;


class PaymentException extends \Exception
{

    /**
     * PaymentException constructor.
     * @param string $message
     */
    public function __construct($message = "")
    {
        parent::__construct($message, 0, null);
    }

}
