<?php

namespace Megacoders\ShoppingBundle\Controller;


use Megacoders\ShoppingBundle\Entity\ShoppingOrder;
use Megacoders\ShoppingBundle\Event\OrderNotificationRequestedEvent;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ShoppingOrderCRUDController
 * @package Megacoders\ShoppingBundle\Controller
 */
class ShoppingOrderCRUDController extends CRUDController
{
    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function confirmAction($id)
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            $this->addFlash('sonata_flash_error', $this->trans(
                'modules.shopping.errors.order_not_found',
                [ '\#%d' => $id]
            ));
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        if ($object->getStatus() != ShoppingOrder::STATUS_CREATED) {
            $this->addFlash('sonata_flash_error', $this->trans(
                'modules.shopping.errors.order_not_allowed_for_confirm',
                [ '\#%d' => $id]
            ));
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $object->setStatus(ShoppingOrder::STATUS_CONFIRMED);
        $this->admin->update($object);

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function cancelAction($id)
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            $this->addFlash('sonata_flash_error', $this->trans(
                'modules.shopping.errors.order_not_found',
                [ '\#%d' => $id]
            ));
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        if ($object->getStatus() == ShoppingOrder::STATUS_CANCELED) {
            $this->addFlash('sonata_flash_error', $this->trans(
                'modules.shopping.errors.order_not_allowed_for_cancel',
                [ '\#%d' => $id]
            ));
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $object->setStatus(ShoppingOrder::STATUS_CANCELED);
        $this->admin->update($object);

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function resendNotificationAction($id)
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            $this->addFlash('sonata_flash_error', $this->trans(
                'modules.shopping.errors.order_not_found',
                [ '\#%d' => $id]
            ));
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $event = new OrderNotificationRequestedEvent($object->getId());
        $this->get('event_dispatcher')->dispatch(OrderNotificationRequestedEvent::NAME, $event);
        $this->addFlash('sonata_flash_success', $this->trans(
            'modules.shopping.messages.order_notification_sent',
            [
                '%d' => $object->getId(),
                '%name' => $object->getAccount()->getFirstName() . ' ' . $object->getAccount()->getLastName(),
                '%mail' => $object->getAccount()->getEmail()
            ]
        ));
        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}