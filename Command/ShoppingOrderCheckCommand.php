<?php

namespace Megacoders\ShoppingBundle\Command;


use Megacoders\ShoppingBundle\Entity\ShoppingOrder;
use Megacoders\ShoppingBundle\Event\OrderCanceledEvent;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ShoppingOrderCheckCommand
 * @package Megacoders\ShoppingBundle\Command
 */
class ShoppingOrderCheckCommand extends ContainerAwareCommand
{

    const DEFAULT_LIFETIME = 10;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager = null;

    /**
     * {@inheritdoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->entityManager = $this->getContainer()->get('doctrine')->getEntityManager('default');
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('megacoders:shopping:cancel-orders')

            // the short description shown while running "php bin/console list"
            ->setDescription('Automatic cancellation of unconfirmed orders')

            ->addArgument('lifetime', InputArgument::OPTIONAL, 'Lifetime of unconfirmed orders in minutes')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $orderLifetime = intval($input->getArgument('lifetime'));
        if ( !$orderLifetime ) {
            $orderLifetime = self::DEFAULT_LIFETIME;
        }

        $date = new \DateTime();
        $date->sub(new \DateInterval('PT' . $orderLifetime . 'M'));

        $qb = $this->entityManager->getRepository('MegacodersShoppingBundle:ShoppingOrder')
            ->createQueryBuilder('o');

        $qb
            ->where($qb->expr()->eq('o.status', ':created_status'))
            ->andWhere( $qb->expr()->lt('o.date', ':date'))
            ->setParameter('created_status', ShoppingOrder::STATUS_CREATED)
            ->setParameter('date', $date->format('Y-m-d H:i:00'))
        ;

        $orders = $qb->getQuery()->getResult();

        foreach ($orders as $order) {
            $order->setStatus(ShoppingOrder::STATUS_CANCELED);
            $this->entityManager->persist($order);
            $this->entityManager->flush();
            $event = new OrderCanceledEvent($order->getId());
            $this->getContainer()->get('event_dispatcher')
                ->dispatch(OrderCanceledEvent::NAME, $event);
        }

    }

}