<?php

namespace Megacoders\ShoppingBundle\Admin;


use Megacoders\AdminBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class ShoppingAccountAdmin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $baseRoutePattern = 'shopping/accounts';

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
    );

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('delete')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, ['label' => 'admin.entities.shopping_account.name'])
            ->add('phone', null, ['label' => 'admin.entities.shopping_account.phone'])
            ->add('email', null, ['label' => 'admin.entities.shopping_account.email'])
            ->add('_action', null, [
                'label' => 'admin.actions._actions',
                'actions' => ['edit' => []]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('firstName', null, ['label' => 'admin.entities.shopping_account.first_name'])
            ->add('lastName', null, ['label' => 'admin.entities.shopping_account.last_name'])
            ->add('phone', null, ['label' => 'admin.entities.shopping_account.phone'])
            ->add('email', EmailType::class, ['label' => 'admin.entities.shopping_account.email'])
        ;
    }
}
