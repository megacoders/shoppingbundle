<?php

namespace Megacoders\ShoppingBundle\Admin;


use Megacoders\AdminBundle\Admin\BaseAdmin;
use Megacoders\AdminBundle\Form\Type\ReadonlyDateTimeType;
use Megacoders\ShoppingBundle\Entity\ShoppingOrder;
use Megacoders\ShoppingBundle\Event\OrderCanceledEvent;
use Megacoders\ShoppingBundle\Event\OrderConfirmedEvent;
use Megacoders\ShoppingBundle\Manager\ShoppingManager;
use Megacoders\ShoppingBundle\Model\ShoppingProduct;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Translation\TranslatorInterface;

class ShoppingOrderAdmin extends BaseAdmin
{
    /**
     * @var string
     */
    protected $baseRoutePattern = 'shopping/orders';

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'date'
    );

    /**
     * @return array
     */
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
            'status' => array (
                'type' => ChoiceType::TYPE_NOT_CONTAINS,
                'value' => [ShoppingOrder::STATUS_CANCELED]
            )
        ), $this->datagridValues);

        return parent::getFilterParameters();
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('date', 'doctrine_orm_date_range', [
                'show_filter' => true,
                'field_type' => 'sonata_type_datetime_range_picker',
                'field_options' => [
                    'field_options' => [
                        'format' => 'dd-MM-yyyy HH:mm'
                    ]
                ]
            ])
            ->add('status', 'doctrine_orm_choice', [
                'show_filter' => true,
                'field_options' => array(
                    'choices' => array_flip(ShoppingOrder::STATUSES_NAMES),
                    'required' => false,
                    'multiple' => true,
                    'expanded' => false,
                ),
                'field_type' => 'choice'
            ])
        ;
    }

    /**
     * @param ShoppingProduct $product
     * @return array
     */
    public function getProductDescription(ShoppingProduct $product)
    {
        /** @var ShoppingManager $shoppingManager */
        $shoppingManager = $this->getConfigurationPool()->getContainer()->get('shopping.manager.shopping_manager');
        return $shoppingManager->getProductDescription($product);
    }

    /**
     * @param ShoppingProduct $product
     * @return bool
     */
    public function isProductQuantityEditable(ShoppingProduct $product)
    {
        /** @var ShoppingManager $shoppingManager */
        $shoppingManager = $this->getConfigurationPool()->getContainer()->get('shopping.manager.shopping_manager');
        return $shoppingManager->isProductQuantityEditable($product);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('delete')
            ->add('confirm', $this->getRouterIdParameter().'/confirm')
            ->add('cancel', $this->getRouterIdParameter().'/cancel')
            ->add('resendNotification', $this->getRouterIdParameter().'/resend-notification')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('date', null, [
                'label' => 'admin.entities.shopping_order.order',
                'template' => 'MegacodersShoppingBundle:Admin/ShoppingOrder:list__date_and_number.html.twig'
            ])
            ->add('user', null, ['label' => 'admin.entities.shopping_order.user'])
            ->add('account', null, [
                'label' => 'admin.entities.shopping_order.account',
                'template' => 'MegacodersShoppingBundle:Admin/ShoppingOrder:list__account.html.twig'
            ])
            ->add('items', null, [
                'label' => 'admin.entities.shopping_order.items',
                'template' => 'MegacodersShoppingBundle:Admin/ShoppingOrder:list__items.html.twig'
            ])
            ->add('payments', null, [
                'label' => 'admin.entities.shopping_order.payments',
                'template' => 'MegacodersShoppingBundle:Admin/ShoppingOrder:list__payments.html.twig',
                'paymentAdmin' => $this->getConfigurationPool()->getContainer()->get('shopping.admin.payment')
            ])
            ->add('_action', null, [
                'label' => 'admin.actions._actions',
                'actions' => [
                    'confirm' => [
                        'template' => 'MegacodersShoppingBundle:CRUD:shopping_order__list__action_confirm.html.twig'
                    ],
                    'edit' => [], 'show' => [], 'delete' => [],
                    'cancel' => [
                        'template' => 'MegacodersShoppingBundle:CRUD:shopping_order__list__action_cancel.html.twig'
                    ],
                ]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var ShoppingOrder $order */
        $order = $this->getSubject();

        /** @var TranslatorInterface $translator */
        $translator = $this->getConfigurationPool()->getContainer()->get('translator');

        $formMapper
            ->with($translator->trans('admin.labels.shopping_order') .($order ? ' #' .$order->getId() : ''))
                ->add('date', ReadonlyDateTimeType::class, [
                    'label' => 'admin.entities.shopping_order.date',
                    'format' => 'yyyy-MM-dd HH:mm'
                ])
                ->add('user', null, ['label' => 'admin.entities.shopping_order.user'])
                ->add('account', null, ['label' => 'admin.entities.shopping_order.account'])
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        /** @var ShoppingOrder $order */
        $order = $this->getSubject();

        /** @var TranslatorInterface $translator */
        $translator = $this->getConfigurationPool()->getContainer()->get('translator');

        $discountService = $this->getDiscountService();

        $showMapper
            ->with($translator->trans('admin.labels.shopping_order') .' #' .$order->getId())
                ->add('date', null, [
                    'label' => 'admin.entities.shopping_order.date',
                    'pattern' => 'yyyy-MM-dd HH:mm'
                ])
                ->add('user', null, ['label' => 'admin.entities.shopping_order.user'])
                ->add('account', null, ['label' => 'admin.entities.shopping_order.account'])
                ->add('statusName', 'trans', ['label' => 'admin.entities.shopping_order.status'])
                ->add('items', null, [
                    'label' => 'admin.entities.shopping_order.items',
                    'template' => 'MegacodersShoppingBundle:Admin/ShoppingOrder:show__items.html.twig'
                ])
                ->add('payments', null, [
                    'label' => 'admin.entities.shopping_order.payments',
                    'template' => 'MegacodersShoppingBundle:Admin/ShoppingOrder:show__payments.html.twig'
                ])
        ;

        if ($discountService) {
            $showMapper->add('discount', null, [
                'label' => 'admin.entities.shopping_order.discount',
                'template' => 'MegacodersShoppingBundle:Admin/ShoppingOrder:show__discount.html.twig',
                'discount' => $discountService->getHelper()->getOverallDiscount($order)
            ]);
        }
        $showMapper
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($order)
    {
        $em = $this->getModelManager()->getEntityManager($this->getClass());
        $original = $em->getUnitOfWork()->getOriginalEntityData($order);

        if ($original['status'] != $order->getStatus()) {

            if ($order->getStatus() == ShoppingOrder::STATUS_CONFIRMED) {
                $event = new OrderConfirmedEvent($order->getId());
                $this->getConfigurationPool()->getContainer()->get('event_dispatcher')
                    ->dispatch(OrderConfirmedEvent::NAME, $event);
            }

            if ($order->getStatus() == ShoppingOrder::STATUS_CANCELED) {
                $event = new OrderCanceledEvent($order->getId());
                $this->getConfigurationPool()->getContainer()->get('event_dispatcher')
                    ->dispatch(OrderCanceledEvent::NAME, $event);
            }

        }

    }

    /**
     * @return array
     */
    public function getExportFields()
    {
        return [
            $this->trans('admin.entities.shopping_order.date') => 'date',
            $this->trans('admin.entities.shopping_account.name') => 'account.name',
            $this->trans('admin.entities.shopping_account.phone') => 'account.phone',
            $this->trans('admin.entities.shopping_account.email') => 'account.email',
            $this->trans('admin.entities.shopping_order.items') => 'items',
            $this->trans('admin.entities.shopping_order.status') => 'status',
            $this->trans('admin.entities.shopping_order.total') => 'price'
        ];
    }

    /**
     * @return array
     */
    public function getExportFormats()
    {
        return array('xls');
    }

    /**
     * @return object
     */
    protected function getDiscountService()
    {
        /** @var ShoppingManager $shoppingManager */
        $shoppingManager = $this->getConfigurationPool()->getContainer()->get('shopping.manager.shopping_manager');
        return $shoppingManager->getDiscountService();
    }

}
