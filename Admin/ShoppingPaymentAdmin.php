<?php

namespace Megacoders\ShoppingBundle\Admin;


use Megacoders\AdminBundle\Admin\BaseAdmin;
use Megacoders\ShoppingBundle\Entity\ShoppingPayment;
use Megacoders\ShoppingBundle\Event\PaymentCreatedEvent;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Translation\TranslatorInterface;

class ShoppingPaymentAdmin extends BaseAdmin
{

    /**
     * {@inheritdoc}
     */
    public function isGranted($name, $object = null)
    {
        if ($name == 'EDIT') {
            return false;
        }

        return true;
    }


    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var TranslatorInterface $translator */
        $translator = $this->getConfigurationPool()->getContainer()->get('translator');

        if ($this->isCreateMode()) {
            $subject = $this->getSubject();
            if ($this->getRequest() && $this->getRequest()->get('order_id')) {
                $order = $this->getConfigurationPool()->getContainer()->get('shopping.manager.shopping_manager')
                    ->findOrder($this->getRequest()->get('order_id'));
                $subject->setOrder($order);
            }
        }

        $formMapper
            ->with($translator->trans('admin.labels.shopping_payment'))
                ->add('date', 'sonata_type_datetime_picker', [
                    'label' => 'admin.entities.shopping_payment.date',
                    'format' => 'dd-MM-yyyy HH:mm'
                ])
                ->add('method', ChoiceType::class, [
                    'label' => 'admin.entities.shopping_payment.method',
                    'choices' => array_flip(ShoppingPayment::METHODS_NAMES)
                ])
                ->add('amount', null, ['label' => 'admin.entities.shopping_payment.amount'])
                ->add('info', null, ['label' => 'admin.entities.shopping_payment.details'])
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($payment)
    {
        if ($payment->getInfo() != null) {
            $payment->setInfo([
                'userComment' => $payment->getInfo()
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPersistentParameters()
    {
        $params = parent::getPersistentParameters();
        if ($this->getRequest()) {
            $params['order_id'] = $this->getRequest()->get('order_id');
        }

        return $params;
    }

    /**
     * {@inheritdoc}
     */
    public function postPersist($payment)
    {
        $event = new PaymentCreatedEvent($payment->getId());
        $this->getConfigurationPool()->getContainer()->get('event_dispatcher')
            ->dispatch(PaymentCreatedEvent::NAME, $event);
    }

}