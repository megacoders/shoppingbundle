<?php

namespace Megacoders\ShoppingBundle\Event;


use Symfony\Component\EventDispatcher\Event;

/**
 * Class OrderNotificationRequestedEvent
 * @package Megacoders\ShoppingBundle\Event
 */
class OrderNotificationRequestedEvent extends Event
{
    const NAME = 'shopping.order.notification.requested';

    /**
     * @var integer
     */
    protected $orderId;

    /**
     * OrderPaidEvent constructor.
     * @param integer $orderId
     */
    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
}