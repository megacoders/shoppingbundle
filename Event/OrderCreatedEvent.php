<?php

namespace Megacoders\ShoppingBundle\Event;


use Symfony\Component\EventDispatcher\Event;

/**
 * Class OrderCreatedEvent
 * @package Megacoders\ShoppingBundle\Event
 */
class OrderCreatedEvent extends Event
{
    const NAME = 'shopping.order.created';

    /**
     * @var integer
     */
    protected $orderId;

    /**
     * OrderCreatedEvent constructor.
     * @param integer $orderId
     */
    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
}