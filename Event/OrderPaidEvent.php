<?php
namespace Megacoders\ShoppingBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class OrderPaidEvent
 * @package Megacoders\ShoppingBundle\Event
 */
class OrderPaidEvent extends Event
{
    const NAME = 'shopping.order.paid';

    /**
     * @var integer
     */
    protected $orderId;

    /**
     * OrderPaidEvent constructor.
     * @param integer $orderId
     */
    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
}