<?php

namespace Megacoders\ShoppingBundle\Event;


use Symfony\Component\EventDispatcher\Event;

/**
 * Class PaymentCreatedEvent
 * @package Megacoders\ShoppingBundle\Event
 */
class PaymentCreatedEvent extends Event
{
    const NAME = 'shopping.payment.created';

    /**
     * @var integer
     */
    protected $paymentId;

    /**
     * PaymentCreatedEvent constructor.
     * @param $paymentId
     */
    public function __construct($paymentId)
    {
        $this->paymentId = $paymentId;
    }

    /**
     * @return int
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }
}