<?php

namespace Megacoders\ShoppingBundle\Event;


use Symfony\Component\EventDispatcher\Event;

/**
 * Class OrderConfirmedEvent
 * @package Megacoders\ShoppingBundle\Event
 */
class OrderConfirmedEvent extends Event
{
    const NAME = 'shopping.order.confirmed';

    /**
     * @var integer
     */
    protected $orderId;

    /**
     * OrderConfirmedEvent constructor.
     * @param integer $orderId
     */
    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
}