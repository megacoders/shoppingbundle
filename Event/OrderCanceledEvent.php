<?php
namespace Megacoders\ShoppingBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class OrderCanceledEvent
 * @package Megacoders\ShoppingBundle\Event
 */
class OrderCanceledEvent extends Event
{
    const NAME = 'shopping.order.canceled';

    /**
     * @var integer
     */
    protected $orderId;

    /**
     * OrderCanceledEvent constructor.
     * @param integer $orderId
     */
    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
}