<?php

namespace Megacoders\ShoppingBundle\Event;


use Symfony\Component\EventDispatcher\Event;

/**
 * Class OrderPendingEvent
 * @package Megacoders\ShoppingBundle\Event
 */
class OrderPendingEvent extends Event
{
    const NAME = 'shopping.order.pending';

    /**
     * @var integer
     */
    protected $orderId;

    /**
     * OrderPendingEvent constructor.
     * @param integer $orderId
     */
    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
}