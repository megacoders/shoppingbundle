<?php

namespace Megacoders\ShoppingBundle\Event;


use Megacoders\ShoppingBundle\Entity\ShoppingOrder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ShoppingPaymentSubscriber
 * @package Megacoders\ShoppingBundle\Event
 */
class ShoppingPaymentSubscriber implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * ShoppingOrderSubscriber constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            PaymentCreatedEvent::NAME => 'onCreatePayment'
        );
    }

    /**
     * @param PaymentCreatedEvent $event
     */
    public function onCreatePayment(PaymentCreatedEvent $event)
    {
        $paymentManager = $this->container->get('shopping.manager.shopping_payment_manager');
        $payment = $paymentManager->findPayment($event->getPaymentId());
        if (!$payment) {
            return;
        }

        $order = $payment->getOrder();
        if ($order->getStatus() != ShoppingOrder::STATUS_PAID) {
            $order->setStatus(ShoppingOrder::STATUS_PAID);
            $em = $this->container->get('doctrine.orm.entity_manager');
            $em->merge($order);
            $em->flush();
        }
    }
}