<?php

namespace Megacoders\ShoppingBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('megacoders_shopping');

        $rootNode
            ->children()
                ->scalarNode('discount_service')
            ->end()
        ;

        return $treeBuilder;
    }
}