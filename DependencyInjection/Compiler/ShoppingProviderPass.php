<?php

namespace Megacoders\ShoppingBundle\DependencyInjection\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ShoppingProviderPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('shopping.manager.shopping_provider_manager')) {
            return;
        }

        $managerDefinition = $container->findDefinition('shopping.manager.shopping_provider_manager');
        $taggedServices = $container->findTaggedServiceIds('shopping.provider');

        foreach ($taggedServices as $id => $tags) {
            $managerDefinition->addMethodCall('registerProvider', [$id, new Reference($id)]);
        }
    }
}
