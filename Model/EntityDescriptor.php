<?php

namespace Megacoders\ShoppingBundle\Model;


class EntityDescriptor
{
    /**
     * @var string
     */
    private $entityClass;

    /**
     * @var int
     */
    private $entityId;

    /**
     * @var string
     */
    private $extra;

    /**
     * EntityDescriptor constructor.
     * @param string $class
     * @param int $id
     * @param string $extra
     */
    public function __construct($class, $id, $extra = "")
    {
        $this->entityClass = $class;
        $this->entityId = $id;
        $this->extra = $extra;
    }

    /**
     * @return string
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }

    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @return string
     */
    public function getExtra(): string
    {
        return $this->extra;
    }

    /**
     * @param string $extra
     * @return EntityDescriptor
     */
    public function setExtra(string $extra)
    {
        $this->extra = $extra;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return implode('#', [$this->entityClass, $this->entityId, $this->extra]);
    }
}
