<?php

namespace Megacoders\ShoppingBundle\Model;


use Symfony\Component\Validator\Constraints as Assert;

abstract class BaseShoppingProduct implements ShoppingProduct
{
    /**
     * @var EntityDescriptor
     */
    private $entityDescriptor;

    /**
     * @Assert\NotBlank()
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $image;

    /**
     * @Assert\NotBlank()
     * @var float
     */
    private $price = 0;

    /**
     * @Assert\NotBlank()
     * @var int
     */
    private $quantity = 1;

    /**
     * @var array
     */
    private $parameters = [];

    /**
     * BaseShoppingProduct constructor.
     * @param EntityDescriptor $entityDescriptor
     */
    public function __construct(EntityDescriptor $entityDescriptor)
    {
        $this->entityDescriptor = $entityDescriptor;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityDescriptor()
    {
        return $this->entityDescriptor;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return BaseShoppingProduct
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return BaseShoppingProduct
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return BaseShoppingProduct
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return BaseShoppingProduct
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return BaseShoppingProduct
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->price * $this->quantity;
    }

    /**
     * {@inheritdoc}
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     * @return BaseShoppingProduct
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getParameter($name, $default = null)
    {
        return isset($this->parameters[$name]) ? $this->parameters[$name] : $default;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return BaseShoppingProduct
     */
    public function setParameter($name, $value)
    {
        $this->parameters[$name] = $value;
        return $this;
    }
}
