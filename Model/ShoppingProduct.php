<?php

namespace Megacoders\ShoppingBundle\Model;


interface ShoppingProduct
{

    /**
     * @return EntityDescriptor
     */
    function getEntityDescriptor();

    /**
     * @return string
     */
    function getName();

    /**
     * @return string|null
     */
    function getDescription();

    /**
     * @return string
     */
    function getImage();

    /**
     * @return float
     */
    function getPrice();

    /**
     * @param float $price
     */
    function setPrice($price);

    /**
     * @return int
     */
    function getQuantity();

    /**
     * @param $quantity
     */
    function setQuantity($quantity);

    /**
     * @return float
     */
    function getTotalPrice();

    /**
     * @return array
     */
    function getParameters();

    /**
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    function getParameter($name, $default = null);

    /**
     * @param array $parameters
     */
    function setParameters(array $parameters);

    /**
     * @param string $name
     * @param mixed $value
     */
    function setParameter($name, $value);

}
