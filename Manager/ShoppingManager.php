<?php

namespace Megacoders\ShoppingBundle\Manager;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Megacoders\ShoppingBundle\Entity\ShoppingAccount;
use Megacoders\ShoppingBundle\Entity\ShoppingOrder;
use Megacoders\ShoppingBundle\Model\ShoppingProduct;
use Megacoders\ShoppingBundle\Provider\ShoppingProvider;
use Megacoders\ShoppingBundle\Service\ShoppingPaymentService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\TranslatorInterface;

class ShoppingManager
{
    /**
     * @var ShoppingProviderManager
     */
    private $providerManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ShoppingPaymentService
     */
    private $paymentService;

    /**
     * @var ShoppingPaymentService
     */
    private $stubPaymentService;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var ShoppingOrderManager
     */
    private $orderManager;

    /**
     * @var object
     */
    private $discountService;

    /**
     * ShoppingManager constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->providerManager = $container->get('shopping.manager.shopping_provider_manager');
        $this->entityManager = $container->get('doctrine.orm.entity_manager');
        $this->paymentService = $container->get('shopping.payment_service');
        $this->stubPaymentService = $container->get('shopping.stub_payment_service');
        $this->translator = $container->get('translator');
        $this->orderManager = $container->get('shopping.manager.shopping_order_manager');

        $this->initDiscountService($container);
    }

    /**
     * @param string $hash
     * @return ShoppingAccount|null
     */
    public function findAccount($hash)
    {
        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository(ShoppingAccount::class);

        /** @var ShoppingAccount $existingAccount */
        $existingAccount = $repository->findOneBy(['hash' => $hash]);

        return $existingAccount;
    }

    /**
     * @param int $orderId
     * @return null|ShoppingOrder
     */
    public function findOrder($orderId)
    {
        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository(ShoppingOrder::class);

        /** @var ShoppingOrder $order */
        $order = $repository->find($orderId);

        if ($order === null) {
            throw new NotFoundHttpException($this->translator->trans('modules.shopping.errors.order_not_found', [$orderId]));
        }

        return $order;
    }

    /**
     * @param ShoppingProduct $product
     * @return array
     */
    public function getProductDescription(ShoppingProduct $product)
    {
        $providers = $this->providerManager->getForEntity($product->getEntityDescriptor()->getEntityClass());

        return array_reduce($providers, function(array $description, ShoppingProvider $provider) use ($product) {
            return array_merge($description, $provider->getProductDescription($product));
        }, []);
    }

    /**
     * @param ShoppingProduct $product
     * @return bool
     */
    public function isProductQuantityEditable(ShoppingProduct $product)
    {
        $providers = $this->providerManager->getForEntity($product->getEntityDescriptor()->getEntityClass());

        return array_reduce($providers, function($editable, ShoppingProvider $provider) use ($product) {
            return $editable && $provider->isProductQuantityEditable($product);
        }, true);
    }

    /**
     * @param ShoppingProduct $product
     * @return bool
     */
    public function isProductParametersEditable(ShoppingProduct $product)
    {
        $providers = $this->providerManager->getForEntity($product->getEntityDescriptor()->getEntityClass());

        return array_reduce($providers, function($editable, ShoppingProvider $provider) use ($product) {
            return $editable && $provider->isProductParametersEditable($product);
        }, true);
    }

    /**
     * @param ShoppingProduct $product
     * @param int             $orderId
     * @return bool
     */
    public function isProductAvailable(ShoppingProduct $product, $orderId)
    {
        $providers = $this->providerManager->getForEntity($product->getEntityDescriptor()->getEntityClass());

        return array_reduce($providers, function($available, ShoppingProvider $provider) use ($product, $orderId) {
            return $available && $provider->isProductAvailable($product, $orderId);
        }, true);
    }

    /**
     * @param ShoppingProduct $product
     * @param array $parameters
     * @return ShoppingProduct
     */
    public function updateProductParameters(ShoppingProduct $product, array $parameters)
    {
        $providers = $this->providerManager->getForEntity($product->getEntityDescriptor()->getEntityClass());

        return array_reduce($providers, function($product, ShoppingProvider $provider) use ($parameters) {
            return $provider->updateProductParameters($product, $parameters);
        }, $product);
    }

    /**
     * @return ShoppingPaymentService
     */
    public function getPaymentService()
    {
        return $this->paymentService;
    }

    /**
     * @return ShoppingPaymentService
     */
    public function getStubPaymentService()
    {
        return $this->stubPaymentService;
    }

    /**
     * @param ShoppingOrder $order
     */
    public function cancelOrder(ShoppingOrder $order)
    {
        $this->orderManager->cancelOrder($order);
    }

    /**
     * @return object
     */
    public function getDiscountService()
    {
        return $this->discountService;
    }

    /**
     * @param ContainerInterface $container
     */
    private function initDiscountService(ContainerInterface $container)
    {
        if ($container->getParameter('megacoders_shopping.discount_service')) {
            try {
                $this->discountService = $container->get($container->getParameter('megacoders_shopping.discount_service'));
            }
            catch (\Exception $e) {}
        }
    }
}
