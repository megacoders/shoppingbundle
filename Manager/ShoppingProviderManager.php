<?php

namespace Megacoders\ShoppingBundle\Manager;


use Megacoders\ShoppingBundle\Provider\ShoppingProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ShoppingProviderManager
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var ShoppingProvider[]
     */
    private $providers = [];

    /**
     * ShoppingBasketProviderManager constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $id
     * @param ShoppingProvider $provider
     */
    public function registerProvider($id, ShoppingProvider $provider)
    {
        $this->providers[$id] = $provider;
    }

    /**
     * @param string $id
     * @return null|ShoppingProvider
     */
    public function get($id)
    {
        return isset($this->providers[$id]) ? $this->providers[$id] : null;
    }

    /**
     * @param string $entityClass
     * @return ShoppingProvider[]
     */
    public function getForEntity($entityClass)
    {
        $providers = [];

        foreach ($this->providers as $provider) {
            if ($provider->supports($entityClass)) {
                $providers[] = $provider;
            }
        }

        return $providers;
    }

    /**
     * @return \Megacoders\ShoppingBundle\Provider\ShoppingProvider[]
     */
    public function getProviders()
    {
        return $this->providers;
    }
}
