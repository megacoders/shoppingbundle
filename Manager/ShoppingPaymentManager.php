<?php

namespace Megacoders\ShoppingBundle\Manager;


use Doctrine\ORM\EntityManager;
use Megacoders\ShoppingBundle\Entity\ShoppingOrder;
use Megacoders\ShoppingBundle\Entity\ShoppingPayment;
use Megacoders\ShoppingBundle\Event\PaymentCreatedEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ShoppingPaymentManager
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var object
     */
    private $eventDispatcher;

    /**
     * AbstractShoppingPaymentService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->entityManager = $container->get('doctrine.orm.entity_manager');
        $this->eventDispatcher = $container->get('event_dispatcher');
    }

    /**
     * @param ShoppingOrder $order
     * @param float         $amount
     * @param string        $method
     * @param []            $info
     * @param string        $serviceUniqueId
     */
    public function createPayment(ShoppingOrder $order, $amount, $method, $info = [], $serviceUniqueId = null)
    {
        $payment = new ShoppingPayment();
        $payment->setOrder($order);
        $payment->setAmount($amount);
        $payment->setMethod($method);
        $payment->setInfo($info);
        $payment->setServiceUniqueId($serviceUniqueId);

        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        $order->addPayment($payment);

        $event = new PaymentCreatedEvent($payment->getId());
        $this->eventDispatcher->dispatch(PaymentCreatedEvent::NAME, $event);
    }

    /**
     * @param int $paymentId
     * @return ShoppingPayment
     */
    public function findPayment($paymentId)
    {
        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository(ShoppingPayment::class);

        /** @var ShoppingPayment $payment */
        $payment = $repository->find($paymentId);

        return $payment;
    }

}