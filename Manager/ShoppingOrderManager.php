<?php

namespace Megacoders\ShoppingBundle\Manager;


use Doctrine\ORM\EntityManager;
use Megacoders\ShoppingBundle\Entity\ShoppingOrder;
use Megacoders\ShoppingBundle\Event\OrderCanceledEvent;
use Megacoders\ShoppingBundle\Event\OrderConfirmedEvent;
use Megacoders\ShoppingBundle\Event\OrderPaidEvent;
use Megacoders\ShoppingBundle\Event\OrderPendingEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ShoppingOrderManager
{
    /**
     * @var object
     */
    private $eventDispatcher;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * ShoppingOrderManager constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->eventDispatcher = $container->get('event_dispatcher');
        $this->entityManager = $container->get('doctrine.orm.entity_manager');
    }

    /**
     * @param ShoppingOrder $order
     */
    public function cancelOrder(ShoppingOrder $order)
    {
        $order->setStatus(ShoppingOrder::STATUS_CANCELED);
        $this->entityManager->persist($order);
        $this->entityManager->flush();
        $event = new OrderCanceledEvent($order->getId());
        $this->eventDispatcher->dispatch(OrderCanceledEvent::NAME, $event);
    }

    /**
     * @param ShoppingOrder $order
     */
    public function confirmOrder(ShoppingOrder $order)
    {
        $order->setStatus(ShoppingOrder::STATUS_CONFIRMED);
        $this->entityManager->persist($order);
        $this->entityManager->flush();
        $event = new OrderConfirmedEvent($order->getId());
        $this->eventDispatcher->dispatch(OrderConfirmedEvent::NAME, $event);
    }

    /**
     * @param ShoppingOrder $order
     */
    public function pendingOrder(ShoppingOrder $order)
    {
        $order->setStatus(ShoppingOrder::STATUS_PENDING);
        $this->entityManager->persist($order);
        $this->entityManager->flush();
        $event = new OrderPendingEvent($order->getId());
        $this->eventDispatcher->dispatch(OrderPendingEvent::NAME, $event);
    }

    /**
     * @param ShoppingOrder $order
     */
    public function payOrder(ShoppingOrder $order)
    {
        $order->setStatus(ShoppingOrder::STATUS_PAID);
        $this->entityManager->persist($order);
        $this->entityManager->flush();
        $event = new OrderPaidEvent($order->getId());
        $this->eventDispatcher->dispatch(OrderPaidEvent::NAME, $event);
    }
}